﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class BallRobot : Controlable
{

	public float force = 10f;
	public float maxSpeed = 10f;

	private Rigidbody rb;

	private void Awake()
	{
		rb = GetComponent<Rigidbody>();
	}

	private void FixedUpdate()
	{
		ProcessInput();
	}

	protected override void DoProcessInput(InputController.InputData inputData)
	{
		rb.AddForce(inputData.movement * force, ForceMode.Acceleration);

		if (rb.velocity.magnitude > maxSpeed)
			rb.velocity = rb.velocity.normalized * maxSpeed;
	}

}
