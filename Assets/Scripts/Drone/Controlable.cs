﻿using UnityEngine;

public abstract class Controlable : MonoBehaviour
{

	public InputController inputController;

	private void FixedUpdate()
	{
		ProcessInput();
	}

	protected virtual void ProcessInput()
	{
		var inputData = inputController == null ? new InputController.InputData() : inputController.currentInput;
		DoProcessInput(inputData);
	}

	protected abstract void DoProcessInput(InputController.InputData inputData);

}
