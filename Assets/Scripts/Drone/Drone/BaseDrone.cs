﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class BaseDrone : Controlable
{

	public float speed = 5f;
	public float maxSpeed = 7.5f;
	public float rotationSpeed = 60;

	private Rigidbody rb;

	private void Awake()
	{
		rb = GetComponent<Rigidbody>();
	}

	protected override void DoProcessInput(InputController.InputData inputData)
	{
		rb.AddForce(inputData.movement * speed, ForceMode.VelocityChange);

		if (rb.velocity.magnitude > maxSpeed)
			rb.velocity = rb.velocity.normalized * maxSpeed;

		// DoLook
		if (inputData.validPosition)
		{
			var targetPosition = inputData.lookPosition;
			targetPosition.y = transform.position.y;

			var toTarget = Quaternion.LookRotation(targetPosition - transform.position, transform.up);
			transform.rotation = Quaternion.RotateTowards(transform.rotation, toTarget, rotationSpeed * Time.fixedDeltaTime);
		}
	}
}
