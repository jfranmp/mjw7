﻿using DG.Tweening;
using UnityEngine;

public class DroneBlink : MonoBehaviour
{

	public Color color = Color.red;
	public float blinkAngle = 60f;

	public float blinkDuration = .05f;
	public Vector2 random = new Vector2(4f, 7f);

	public Light spotLight;
	public new Renderer renderer;

	private Material material;

	private float m_nextBlink;

	private void Awake()
	{
		material = renderer.material;
		m_nextBlink = Time.time + Random.Range(random.x, random.y);
	}

	private void Update()
	{
		if (Time.time > m_nextBlink)
		{
			Blink();
		}
	}

	public void Blink()
	{
		Sequence s = DOTween.Sequence();
		s.Append(material.DOColor(Color.black, blinkDuration / 2f));
		s.Join(DOTween.To(() => spotLight.spotAngle, x => spotLight.spotAngle = x, 0, blinkDuration / 2f));
		s.Append(material.DOColor(color, blinkDuration / 2f));
		s.Join(DOTween.To(() => spotLight.spotAngle, x => spotLight.spotAngle = x, blinkAngle, blinkDuration / 2f));

		m_nextBlink = Time.time + Random.Range(random.x, random.y);
	}
}
