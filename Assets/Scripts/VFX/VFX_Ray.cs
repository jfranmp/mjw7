﻿using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(LineRenderer))]
public class VFX_Ray : MonoBehaviour {

	public Transform endPoint;
	public int segmentCount = 11;

	public float noiseAmp = 1;
	public float noiseFreq = 5f;

	private Vector3[] points;
	private LineRenderer linerenderer;

	private void Awake()
	{
		linerenderer = GetComponent<LineRenderer>();
		points = new Vector3[segmentCount];
	}

	private void Update()
	{
		UpdateLine();
	}

	private void UpdateLine()
	{
		if (endPoint == null) return;

		float seed = Random.Range(0f, 1f);
		float noise;
		for (float i = 0; i < segmentCount; i++)
		{
			noise = Mathf.PerlinNoise(Time.time * noiseFreq + (i / (segmentCount - 1)), seed) * noiseAmp;

			Vector3 basePoint = Vector3.Lerp(transform.position, endPoint.position, i / (segmentCount - 1));
			basePoint = basePoint + Vector3.up * noise + Vector3.right * noise;
			points[(int)i] = basePoint;
		}

		linerenderer.positionCount = points.Length;
		linerenderer.SetPositions(points);
	}
}
