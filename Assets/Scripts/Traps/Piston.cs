﻿using CF.Utils;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;

public class Piston : MonoBehaviour
{

	public bool moveToEndPosition;

	[Header("Configuration")]
	public Vector3 startLocal;
	public Vector3 endLocal;

	[Header("Tween")]
	public float toStartDuration = 2f;
	public Ease toStartEase = Ease.InOutSine;

	public float toEndDuration = .4f;
	public Ease toEndEase = Ease.OutCubic;

	[Header("Events")]
	public UnityEvent OnOpenDoor = new UnityEvent();
	public UnityEvent OnCloseDoor = new UnityEvent();

	private bool m_isInEndPosition;

	private SafeTween m_movementTween = new SafeTween(SafeTween.SafeTweenPriority.New, SafeTween.SafeTweenMode.Kill);

	#region Unity Events

	private void Update()
	{
		if (m_isInEndPosition != moveToEndPosition && !m_movementTween.IsPlaying)
		{
			if (moveToEndPosition) ToEnd();
			else ToStart();
		}
	}

	#endregion

	#region Public API

	public void ToStart()
	{
		m_isInEndPosition = false;
		m_movementTween.Set(transform.DOLocalMove(startLocal, toStartDuration).SetEase(toStartEase).OnComplete(OnOpenDoor.Invoke));
	}

	public void ToEnd()
	{
		m_isInEndPosition = true;
		m_movementTween.Set(transform.DOLocalMove(endLocal, toEndDuration).SetEase(toEndEase).OnComplete(OnCloseDoor.Invoke));
	}

	#endregion

	#region Unity Helpers

	[ContextMenu("Set Start Position")]
	private void SetStartPosition()
	{
		startLocal = transform.localPosition;
	}

	[ContextMenu("Set End Position")]
	private void SetEndPosition()
	{
		endLocal = transform.localPosition;
	}

	[ContextMenu("Move to Start Position")]
	private void MoveToStart()
	{
		m_isInEndPosition = false;
		transform.localPosition = startLocal;
	}

	[ContextMenu("Move to end Position")]
	private void MoveToEnd()
	{
		m_isInEndPosition = true;
		transform.localPosition = endLocal;
	}

	#endregion

}
