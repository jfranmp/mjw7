﻿using UnityEngine;

public class InputController : MonoBehaviour
{

	public InputData currentInput;

	private void FixedUpdate()
	{
		ProcessInput();
	}

	private void ProcessInput()
	{
		Vector3 movement = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));

		RaycastHit hit;
		if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 100f))
		{
			currentInput = new InputData(movement, true, hit.point, Input.GetMouseButtonDown(0), Input.GetMouseButton(0), Input.GetMouseButtonDown(1), Input.GetMouseButton(1));
		}
		else
		{
			currentInput = new InputData(movement, false, hit.point, Input.GetMouseButtonDown(0), Input.GetMouseButton(0), Input.GetMouseButtonDown(1), Input.GetMouseButton(1));
		}
	}

	[System.Serializable]
	public struct InputData
	{
		public Vector3 movement;

		public bool validPosition;
		public Vector3 lookPosition;

		public bool actionADown;
		public bool actionAPressed;
		public bool actionBDown;
		public bool actionBPressed;

		public InputData(Vector3 movement, bool validPosition, Vector3 lookPosition, bool actionADown, bool actionAPressed, bool actionBDown, bool actionBPressed)
		{
			this.movement = movement;

			this.validPosition = validPosition;
			this.lookPosition = lookPosition;

			this.actionADown = actionADown;
			this.actionAPressed = actionAPressed;

			this.actionBDown = actionBDown;
			this.actionBPressed = actionBPressed;
		}
	}
}
