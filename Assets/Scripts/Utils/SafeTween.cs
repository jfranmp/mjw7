﻿using DG.Tweening;

namespace CF.Utils
{
	public class SafeTween
	{
		public SafeTweenPriority Directive { get; set; }
		public SafeTweenMode Mode { get; set; }
		public Tween CurrentTween { get; private set; }

		public bool IsPlaying { get { return CurrentTween != null && CurrentTween.IsPlaying(); } }

		public SafeTween(SafeTweenPriority directive, SafeTweenMode mode, Tween currentTween = null)
		{
			Directive = directive;
			CurrentTween = currentTween;
		}

		#region [Public API]

		public Tween Set(Tween tween)
		{
			switch (Directive)
			{
				case SafeTweenPriority.Old:
					if (IsPlaying)
						DestroyTween(tween);
					else
						CurrentTween = tween;
					break;
				case SafeTweenPriority.New:
					if (IsPlaying)
						DestroyTween(CurrentTween);

					CurrentTween = tween;
					break;
			}

			return CurrentTween;
		}

		public void Kill()
		{
			if (CurrentTween != null)
				CurrentTween.Kill();
		}

		public void Complete()
		{
			if (CurrentTween != null)
				CurrentTween.Complete();
		}

		public enum SafeTweenPriority
		{
			New, Old
		}

		public enum SafeTweenMode
		{
			Kill, Complete
		}

		#endregion

		#region Helper

		private void DestroyTween(Tween tween)
		{
			switch (Mode)
			{
				case SafeTweenMode.Kill:
					tween.Kill();
					break;
				case SafeTweenMode.Complete:
					tween.Complete();
					break;
			}
		}

		#endregion

	}
}