﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace CF.Events
{
	public class Sensor : MonoBehaviour
	{
		public List<string> typesName = new List<string>();
		public List<Component> Inside = new List<Component>();
		public int Count { get { return Inside.Count; } }

		private List<Type> m_types = new List<Type>();

		public UnityEvent OnEnter = new UnityEvent();
		public UnityEvent OnExit = new UnityEvent();
		public UnityEvent OnChange = new UnityEvent();
		public UnityEvent OnSomethingInside = new UnityEvent();
		public UnityEvent OnNothingInside = new UnityEvent();

		private int m_lastCount;

		#region Unity Events

		private void Awake()
		{
			foreach (var t in typesName)
			{
				var currentType = Type.GetType(t, false, true);
				if (!m_types.Contains(currentType) && currentType.IsSubclassOf(typeof(Component)))
				{
					m_types.Add(currentType);
				}
			}
		}

		private void OnTriggerEnter(Collider other)
		{
			CheckInside();

			foreach (var type in m_types)
			{
				var obj = other.GetComponent(type) as Component;
				if (obj != null)
				{
					if (!Inside.Contains(obj))
					{
						Inside.Add(obj);

						// Events
						OnEnter.Invoke();
						OnChange.Invoke();
						CheckEvents();
					}

					return;
				}
			}
		}

		private void OnTriggerExit(Collider other)
		{
			CheckInside();

			foreach (var type in m_types)
			{
				var obj = other.GetComponent(type) as Component;
				if (obj != null)
				{
					if (Inside.Contains(obj))
					{
						Inside.Remove(obj);

						// Events
						OnExit.Invoke();
						OnChange.Invoke();
						CheckEvents();
					}

					return;
				}
			}
		}

		#endregion

		#region Helpers

		private void CheckEvents()
		{
			if (m_lastCount == 0 && Count > 0) OnSomethingInside.Invoke();
			if (m_lastCount > 0 && Count == 0) OnNothingInside.Invoke();

			m_lastCount = Count;
		}

		private void CheckInside()
		{
			Inside = Inside.FindAll(x => x != null);
		}

		#endregion

	}

	public class Sensor<T> : MonoBehaviour where T : MonoBehaviour
	{

		public List<T> Inside = new List<T>();
		public int Count { get { return Inside.Count; } }

		public UnityEvent OnEnter = new UnityEvent();
		public UnityEvent OnExit = new UnityEvent();
		public UnityEvent OnChange = new UnityEvent();
		public UnityEvent OnSomethingInside = new UnityEvent();
		public UnityEvent OnNothingInside = new UnityEvent();

		private int m_lastCount;

		#region Unity Events

		private void OnTriggerEnter(Collider other)
		{
			CheckInside();

			T obj = other.GetComponent<T>();
			if (obj != null && !Inside.Contains(obj))
			{
				Inside.Add(obj);

				// Events
				OnEnter.Invoke();
				OnChange.Invoke();
				CheckEvents();
			}
		}

		private void OnTriggerExit(Collider other)
		{
			CheckInside();

			T obj = other.GetComponent<T>();
			if (obj != null && Inside.Contains(obj))
			{
				Inside.Remove(obj);

				// Events
				OnExit.Invoke();
				OnChange.Invoke();
				CheckEvents();
			}
		}

		#endregion

		#region Helpers

		private void CheckEvents()
		{
			if (m_lastCount == 0 && Count > 0) OnSomethingInside.Invoke();
			if (m_lastCount > 0 && Count == 0) OnNothingInside.Invoke();

			m_lastCount = Count;
		}

		private void CheckInside()
		{
			Inside = Inside.FindAll(x => x != null);
		}

		#endregion

	}

	public class Sensor<T, U> : MonoBehaviour where T : MonoBehaviour where U : MonoBehaviour
	{

		public List<T> InsideT = new List<T>();
		public List<U> InsideU = new List<U>();

		public int Count { get { return InsideT.Count + InsideU.Count; } }

		private int m_lastCount;

		public UnityEvent OnEnter = new UnityEvent();
		public UnityEvent OnExit = new UnityEvent();
		public UnityEvent OnChange = new UnityEvent();
		public UnityEvent OnSomethingInside = new UnityEvent();
		public UnityEvent OnNothingInside = new UnityEvent();

		#region Unity Events

		private void OnTriggerEnter(Collider other)
		{
			CheckInside();

			T objT = other.GetComponent<T>();
			U objU = other.GetComponent<U>();

			if (objT != null) OnTriggerEnterT(objT);
			else if (objU != null) OnTriggerEnterU(objU);
		}

		private void OnTriggerExit(Collider other)
		{
			CheckInside();

			T objT = other.GetComponent<T>();
			U objU = other.GetComponent<U>();

			if (objT != null) OnTriggerExitT(objT);
			else if (objU != null) OnTriggerExitU(objU);
		}

		#endregion

		#region Helpers

		protected virtual void OnTriggerEnterT(T obj)
		{
			if (!InsideT.Contains(obj))
			{
				InsideT.Add(obj);

				// Events
				OnEnter.Invoke();
				OnChange.Invoke();
				CheckEvents();
			}
		}

		protected virtual void OnTriggerEnterU(U obj)
		{
			if (!InsideU.Contains(obj))
			{
				InsideU.Add(obj);

				// Events
				OnEnter.Invoke();
				OnChange.Invoke();
				CheckEvents();
			}
		}

		protected virtual void OnTriggerExitT(T obj)
		{
			if (InsideT.Contains(obj))
			{
				InsideT.Remove(obj);

				// Events
				OnExit.Invoke();
				OnChange.Invoke();
				CheckEvents();
			}
		}

		protected virtual void OnTriggerExitU(U obj)
		{
			if (InsideU.Contains(obj))
			{
				InsideU.Remove(obj);

				// Events
				OnExit.Invoke();
				OnChange.Invoke();
				CheckEvents();
			}
		}

		private void CheckInside()
		{
			InsideT = InsideT.FindAll(x => x != null);
			InsideU = InsideU.FindAll(x => x != null);
		}

		private void CheckEvents()
		{
			if (m_lastCount == 0 && Count > 0)
			{
				OnSomethingInside.Invoke();
			}
			if (m_lastCount > 0 && Count == 0)
			{
				OnNothingInside.Invoke();
			}

			m_lastCount = Count;
		}

		#endregion

	}


	public class Sensor<T, U, V> : MonoBehaviour where T : MonoBehaviour where U : MonoBehaviour where V : MonoBehaviour
	{

		public List<T> InsideT = new List<T>();
		public List<U> InsideU = new List<U>();
		public List<V> InsideV = new List<V>();

		public int Count { get { return InsideT.Count + InsideU.Count + InsideV.Count; } }

		private int m_lastCount;

		public UnityEvent OnEnter = new UnityEvent();
		public UnityEvent OnExit = new UnityEvent();
		public UnityEvent OnChange = new UnityEvent();
		public UnityEvent OnSomethingInside = new UnityEvent();
		public UnityEvent OnNothingInside = new UnityEvent();

		#region Unity Events

		private void OnTriggerEnter(Collider other)
		{
			CheckInside();

			T objT = other.GetComponent<T>();
			U objU = other.GetComponent<U>();
			V objV = other.GetComponent<V>();

			if (objT != null) OnTriggerEnterT(objT);
			else if (objU != null) OnTriggerEnterU(objU);
			else if (objV != null) OnTriggerEnterV(objV);
		}

		private void OnTriggerExit(Collider other)
		{
			CheckInside();

			T objT = other.GetComponent<T>();
			U objU = other.GetComponent<U>();
			V objV = other.GetComponent<V>();

			if (objT != null) OnTriggerExitT(objT);
			else if (objU != null) OnTriggerExitU(objU);
			else if (objV != null) OnTriggerExitV(objV);
		}

		#endregion

		#region Helpers

		protected virtual void OnTriggerEnterT(T obj)
		{
			if (!InsideT.Contains(obj))
			{
				InsideT.Add(obj);

				// Events
				OnEnter.Invoke();
				OnChange.Invoke();
				CheckEvents();
			}
		}

		protected virtual void OnTriggerEnterU(U obj)
		{
			if (!InsideU.Contains(obj))
			{
				InsideU.Add(obj);

				// Events
				OnEnter.Invoke();
				OnChange.Invoke();
				CheckEvents();
			}
		}

		protected virtual void OnTriggerEnterV(V obj)
		{
			if (!InsideV.Contains(obj))
			{
				InsideV.Add(obj);

				// Events
				OnEnter.Invoke();
				OnChange.Invoke();
				CheckEvents();
			}
		}

		protected virtual void OnTriggerExitT(T obj)
		{
			if (InsideT.Contains(obj))
			{
				InsideT.Remove(obj);

				// Events
				OnExit.Invoke();
				OnChange.Invoke();
				CheckEvents();
			}
		}

		protected virtual void OnTriggerExitU(U obj)
		{
			if (InsideU.Contains(obj))
			{
				InsideU.Remove(obj);

				// Events
				OnExit.Invoke();
				OnChange.Invoke();
				CheckEvents();
			}
		}

		protected virtual void OnTriggerExitV(V obj)
		{
			if (InsideV.Contains(obj))
			{
				InsideV.Remove(obj);

				// Events
				OnExit.Invoke();
				OnChange.Invoke();
				CheckEvents();
			}
		}

		private void CheckInside()
		{
			InsideT = InsideT.FindAll(x => x != null);
			InsideU = InsideU.FindAll(x => x != null);
			InsideV = InsideV.FindAll(x => x != null);
		}

		private void CheckEvents()
		{
			if (m_lastCount == 0 && Count > 0)
			{
				OnSomethingInside.Invoke();
			}
			if (m_lastCount > 0 && Count == 0)
			{
				OnNothingInside.Invoke();
			}

			m_lastCount = Count;
		}

		#endregion

	}
}