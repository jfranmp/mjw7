﻿using System;

namespace CF.Utils.Events
{

	public class EventX
	{
		public string EventName { get; private set; }
		private event Action OnEvent;

		public EventX(string EventName = "event")
		{
			this.EventName = EventName;
		}

		#region operators

		public static EventX operator +(EventX e, Action action)
		{
			e.AddListener(action);
			return e;
		}

		public static EventX operator -(EventX e, Action action)
		{
			e.RemoveListener(action);
			return e;
		}

		public static implicit operator Action(EventX e)
		{
			return e.OnEvent;
		}

		#endregion

		#region [Public API]

		public EventX AddListener(Action listener)
		{
			if (OnEvent == null)
				OnEvent = listener;
			else
				OnEvent += listener;

			return this;
		}

		public void RemoveListener(Action listener)
		{
			OnEvent -= listener;
		}

		public EventX AddOnce(Action listener)
		{
			Action newListener = null;
			newListener = () =>
			{
				RemoveListener(listener);
				listener();
			};

			return AddListener(newListener);
		}

		public void Invoke()
		{
			if (OnEvent != null)
				OnEvent();
		}

		#endregion
	}

	public class EventX<T>
	{
		public string EventName { get; private set; }
		private event Action<T> OnEvent;

		public EventX() : this("event")
		{

		}

		public EventX(string EventName)
		{
			this.EventName = EventName;
		}

		#region operators

		public static EventX<T> operator +(EventX<T> e, Action<T> action)
		{
			e.AddListener(action);
			return e;
		}

		public static EventX<T> operator -(EventX<T> e, Action<T> action)
		{
			e.RemoveListener(action);
			return e;
		}

		public static implicit operator Action<T>(EventX<T> e)
		{
			return e.OnEvent;
		}

		#endregion

		public EventX<T> AddListener(Action<T> listener)
		{
			if (OnEvent == null)
				OnEvent = listener;
			else
				OnEvent += listener;

			return this;
		}

		public void RemoveListener(Action<T> listener)
		{
			OnEvent -= listener;
		}

		public EventX<T> AddOnce(Action<T> listener)
		{
			Action<T> newListener = null;
			newListener = x =>
			{
				RemoveListener(listener);
				listener(x);
			};

			return AddListener(newListener);
		}

		public void Invoke(T param1)
		{
			if (OnEvent != null)
				OnEvent(param1);
		}
	}

	public class EventX<T, U>
	{
		public string EventName { get; private set; }
		private event Action<T, U> OnEvent;

		public EventX() : this("event")
		{

		}


		public EventX(string EventName)
		{
			this.EventName = EventName;
		}

		#region operators

		public static EventX<T, U> operator +(EventX<T, U> e, Action<T, U> action)
		{
			e.AddListener(action);
			return e;
		}

		public static EventX<T, U> operator -(EventX<T, U> e, Action<T, U> action)
		{
			e.RemoveListener(action);
			return e;
		}

		public static implicit operator Action<T, U>(EventX<T, U> e)
		{
			return e.OnEvent;
		}

		#endregion

		public EventX<T, U> AddListener(Action<T, U> listener)
		{
			if (OnEvent == null)
				OnEvent = listener;
			else
				OnEvent += listener;

			return this;
		}

		public void RemoveListener(Action<T, U> listener)
		{
			OnEvent -= listener;
		}

		public EventX<T, U> AddOnce(Action<T, U> listener)
		{
			Action<T, U> newListener = null;
			newListener = (x, y) =>
			{
				RemoveListener(listener);
				listener(x, y);
			};

			return AddListener(newListener);
		}

		public void Invoke(T param1, U param2)
		{
			if (OnEvent != null)
				OnEvent(param1, param2);
		}
	}

	public class EventX<T, U, V>
	{
		public string EventName { get; private set; }
		private event Action<T, U, V> OnEvent;

		public EventX() : this("event")
		{

		}


		public EventX(string EventName)
		{
			this.EventName = EventName;
		}

		#region operators

		public static EventX<T, U, V> operator +(EventX<T, U, V> e, Action<T, U, V> action)
		{
			e.AddListener(action);
			return e;
		}

		public static EventX<T, U, V> operator -(EventX<T, U, V> e, Action<T, U, V> action)
		{
			e.RemoveListener(action);
			return e;
		}

		public static implicit operator Action<T, U, V>(EventX<T, U, V> e)
		{
			return e.OnEvent;
		}

		#endregion

		public EventX<T, U, V> AddListener(Action<T, U, V> listener)
		{
			if (OnEvent == null)
				OnEvent = listener;
			else
				OnEvent += listener;

			return this;
		}

		public void RemoveListener(Action<T, U, V> listener)
		{
			OnEvent -= listener;
		}

		public EventX<T, U, V> AddOnce(Action<T, U, V> listener)
		{
			Action<T, U, V> newListener = null;
			newListener = (x, y, z) =>
			{
				RemoveListener(listener);
				listener(x, y, z);
			};

			return AddListener(newListener);
		}

		public void Invoke(T param1, U param2, V param3)
		{
			if (OnEvent != null)
				OnEvent(param1, param2, param3);
		}
	}

}