﻿using UnityEngine;

public class Vibration : MonoBehaviour
{

	public Vector3 noiseAmount = new Vector3(0, .1f, 0);
	public Vector3 frequency = new Vector3(0, 10, 0);
	public float smooth = .2f;

	private Transform _myTransform;
	private Vector3 offset;
	private Vector3 _origin;

	private void Start()
	{
		_myTransform = transform;
		_origin = _myTransform.localPosition;

		offset.x = Random.Range(-1f, 1f);
		offset.y = Random.Range(-1f, 1f);
		offset.z = Random.Range(-1f, 1f);
	}

	private void Update()
	{
		Vector3 startPosition = _myTransform.localPosition;
		Vector3 endPosition = _origin + new Vector3((2 * Mathf.PerlinNoise(Time.time * frequency.x, .13f + offset.x) - 1f) * noiseAmount.x,
													 (2 * Mathf.PerlinNoise(Time.time * frequency.y, .27f + offset.y) - 1f) * noiseAmount.y,
													 (2 * Mathf.PerlinNoise(Time.time * frequency.z, .73f + offset.z) - 1f) * noiseAmount.z);

		_myTransform.localPosition = Vector3.Lerp(startPosition, endPosition, smooth);
	}
}